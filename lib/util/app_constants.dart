import 'package:flutter/material.dart';

class AppConstants {
  static final AppConstants _instance = new AppConstants._internal();

  factory AppConstants() {
    return _instance;
  }

  static AppConstants get() {
    return _instance;
  }

  AppConstants._internal() {}

  static MaterialColor appBarColor = const MaterialColor(
    0xFFFFFFFF,
    const <int, Color>{
      50: const Color(0xFFFFFFFF),
      100: const Color(0xFFFFFFFF),
      200: const Color(0xFFFFFFFF),
      300: const Color(0xFFFFFFFF),
      400: const Color(0xFFFFFFFF),
      500: const Color(0xFFFFFFFF),
      600: const Color(0xFFFFFFFF),
      700: const Color(0xFFFFFFFF),
      800: const Color(0xFFFFFFFF),
      900: const Color(0xFFFFFFFF),
    },
  );

  final String appName = 'whatAZoo';

  final Color primaryColor = Colors.grey;
  final Color accentColor = Color(0xFF455a74);
  final Color accentBlack = Color(0xFF0C0F0A);
  final Color accentWhite = Color(0xFFECF2FB);
  final Color periwinkleGray = Color(0xFFB0C6E1);
  final Color tuna = Color(0xFF303035);
  final Color calypso = Color(0xFF143b58);
  final Color calypsoLight = Color(0xFF909da8);
  final Color sweetPink = Color(0xFFFE9EB1);

  final Color white = Color(0xFFfafbfd);

  final String placeholder =
      "http://akademiaoze.pwsz.legnica.edu.pl/wp-content/uploads/2014/02/logouczelni.png";
  final String brandTextLogo = "WhatAZoo";

  var randomImage = [
    'https://images.unsplash.com/photo-1568965081720-a2eadffaa3e4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1530728327726-b504480e42ec?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1561381484-be7f07027828?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1524272332618-3a94122bb0c1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1554554300-4657bda4d97a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1446562506700-1880c71a3a96?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1504850759690-1c3beb59be7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1475265582030-e0247dbdc82c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1524272332618-3a94122bb0c1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80',
    'https://images.unsplash.com/photo-1552071379-041b32707fed?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1519066629447-267fffa62d4b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1417721885406-d31aee8c2a79?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1458832463097-ab73e7b4fc4f?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1565801802606-9923415ee1eb?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1462060336776-cef9892ad536?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1515444347446-4380c4d8a6ed?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1572968253691-e233e0084088?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1532946735282-1e74836c1625?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1474917299080-1371d7175b62?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1466477234737-8a3b3faed8c3?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1565772514232-e8c790492002?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1577907500165-2c31fabff2da?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1564862072807-cc44408b48e9?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1528313316397-ddee14ed98f0?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1445583934509-4ad5ffe6ef08?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1445820200644-69f87d946277?ixlib=rb-1'
        '.2.1&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1495368008737-9d436e094dd5?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1459262838948-3e2de6c1ec80?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1475874619827-b5f0310b6e6f?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
    'https://images.unsplash.com/photo-1477764085876-1df3917e052f?ixlib=rb-1'
        '.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60',
  ];
}
