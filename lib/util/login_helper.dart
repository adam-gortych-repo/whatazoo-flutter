import 'package:whatazoo_flutter/model/friendslist.dart';
import 'package:whatazoo_flutter/model/user.dart';
import 'package:whatazoo_flutter/model/users.dart';

class LoginHelper{
  static UserData loggedUser;
  static UsersResponse usersListData;
  static FriendsListResponse userFriends;
}