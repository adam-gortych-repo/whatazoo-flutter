import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

import 'app_constants.dart';

class CommonWidgets {
  static Widget buildTextPlaceholder() {
    return Center(
      child: Text('Hello world text placeholder'),
    );
  }

  static void forceOrientation(bool isPortrait) {
    if (isPortrait) {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.portraitUp,
      ]);
    } else {
      SystemChrome.setPreferredOrientations([
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight,
      ]);
    }
  }

  static Widget modalLoading({Color backgroundColor = Colors.grey}) {
    return new Stack(
      children: [
        new Opacity(
          opacity: 0.3,
          child: ModalBarrier(dismissible: false, color: backgroundColor),
        ),
        new Center(
          child: new CircularProgressIndicator(),
        ),
      ],
    );
  }

  static Widget loading() {
    return const Center(
      child: CircularProgressIndicator(),
    );
  }

  static Widget getInputField(TextInputType type,
      TextEditingController textController, String labelText, String hintText,
      {bool autofocus = false,
      bool hideText = false,
      int maxLines = 1,
      bool enabled = true}) {
    return TextFormField(
      keyboardType: type,
      style: StyleHelper.get().regularTextStyle(fontWeight: FontWeight.bold),
      autofocus: autofocus,
      controller: textController,
      obscureText: hideText,
      enabled: enabled,
      minLines: 1,
      maxLines: maxLines,
      decoration: InputDecoration(
        hintText: labelText,
        labelText: hintText,
        labelStyle:
            StyleHelper.get().regularTextStyle(fontWeight: FontWeight.w300),
        focusedBorder: new UnderlineInputBorder(
            borderSide: BorderSide(color: AppConstants.get().calypsoLight)),
        contentPadding: EdgeInsets.fromLTRB(0.0, 4.0, 8.0, 4.0),
      ),
    );
  }

  static Widget zooTextInputField(String hintText) {
    return TextField(
      style: StyleHelper.get().regularTextStyle(),
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: StyleHelper.get().regularTextStyle(),
      ),
    );
  }

  static Widget zooButton(String text, Function onClicked) {
    return Container(
      height: 64,
      width: 400,
      child: MaterialButton(
        color: AppConstants.get().sweetPink,
        child: Text(
          text,
          style: TextStyle(color: Colors.white, fontSize: 16),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () => onClicked,
      ),
    );
  }
}
