import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/communication/error_reponse.dart';

class NetworkUtil {
  static NetworkUtil _instance = new NetworkUtil.internal();

  NetworkUtil.internal();

  factory NetworkUtil() => _instance;

  final JsonDecoder _decoder = new JsonDecoder();
  final Duration timeout = new Duration(seconds: 30);

  Future<dynamic> get(dynamic url) async {
    //url string or Uri
    debugPrint("API GET URL: " + url);
    debugPrint("API GET headers: " + ApiService.getHeaders().toString());
    return http
        .get(url, headers: ApiService.getHeaders())
        .timeout(timeout)
        .then((http.Response response) {
      try {
        final String res = response.body;
        final int statusCode = response.statusCode;
        debugPrint("API $statusCode GET URL: " + url);
        if (statusCode < 200 || statusCode >= 400 || json == null) {
          return ErrorResponse.fromJson(_decoder.convert(response.body));
        }
        return _decoder.convert(res);
      } catch (exception) {
        return ErrorResponse(title: "Error Parsing", success: false);
      }
    }, onError: (e) {
      return ErrorResponse(title: "Unhandled Exception", success: false);
    });
  }

  Future<dynamic> post(dynamic url, {body, encoding}) async {
    //url string or Uri
    debugPrint("API POST URL: " + url);
    debugPrint("API POST headers: " + ApiService.getHeaders().toString());
    return http
        .post(url,
            body: body, headers: ApiService.getHeaders(), encoding: encoding)
        .timeout(timeout)
        .then((http.Response response) {
      try {
        final String res = response.body;
        final int statusCode = response.statusCode;
        debugPrint("API $statusCode POST URL: " + url);

        if (statusCode < 200 || statusCode >= 400 || json == null) {
          return ErrorResponse.fromJson(_decoder.convert(response.body));
        }
        return _decoder.convert(res);
      } catch (exception) {
        return ErrorResponse(title: "Error Parsing", success: false);
      }
    }, onError: (e) {
      return ErrorResponse(title: "Unhandled Exception", success: false);
    });
  }
}
