import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';

class StyleHelper {
  static final StyleHelper _instance = new StyleHelper._internal();

  factory StyleHelper() {
    return _instance;
  }

  static StyleHelper get() {
    return _instance;
  }

  StyleHelper._internal() {}

  TextStyle headerTextStyle(
          {double opacityValue = 1.0,
          FontWeight fontWeight = FontWeight.normal}) =>
      TextStyle(
          fontSize: 30,
          color: AppConstants.get().accentColor,
          fontWeight: fontWeight);

  TextStyle regularTextStyle(
          {double opacityValue = 1.0,
          FontWeight fontWeight = FontWeight.normal}) =>
      TextStyle(
          fontSize: 14,
          color: AppConstants.get().calypso.withOpacity(opacityValue),
          fontWeight: fontWeight);

  TextStyle headlineTextStyle(
          {double opacityValue = 1.0,
          FontWeight fontWeight = FontWeight.normal}) =>
      TextStyle(
          fontSize: 20,
          color: AppConstants.get().calypso,
          fontWeight: fontWeight);

  TextStyle headlineGreyTextStyle(
          {double opacityValue = 1.0,
          FontWeight fontWeight = FontWeight.normal}) =>
      TextStyle(
          fontSize: 20,
          color: AppConstants.get().calypsoLight,
          fontWeight: fontWeight);
}
