import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/communication/error_reponse.dart';
import 'package:whatazoo_flutter/model/zoo.dart';
import 'package:whatazoo_flutter/page/home_page.dart';
import 'package:whatazoo_flutter/page/login_page.dart';
import 'package:whatazoo_flutter/page/profile_community_page.dart';
import 'package:whatazoo_flutter/page/profile_friends_page.dart';
import 'package:whatazoo_flutter/page/register_page.dart';
import 'package:whatazoo_flutter/page/zoo_details_page.dart';

class RoutingUtils {
  static final String home = '/HomedPage';
  static final String login = '/LoginPage';
  static final String register = '/RegisterPage';

  static final Map<String, WidgetBuilder> allRoutes = <String, WidgetBuilder>{
    login: (BuildContext context) => new LoginPage(title: 'Login Page'),
    register: (BuildContext context) =>
        new RegisterPage(title: 'Register Page'),
    home: (BuildContext context) => new HomePage(title: 'Home Page'),
  };

  static goToZooDetails(BuildContext context, Zoos zoos, String zooImageUrl) {
    Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => ZooDetailsPage(
              title: 'Details',
              zoos: zoos,
              zooImageUrl: zooImageUrl,
            )));
  }

  static goToFriendsListPage(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ProfileFriends()));
  }

  static goToUsersListPage(BuildContext context) {
    Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => ProfileUsersPage()));
  }

  static showErrorDialog(BuildContext context, {String title, String message}) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) => AlertDialog(
        title: new Text(title),
        content: new Text(message),
      ),
    );
  }

  static showErrorResponseDialog(
      BuildContext context, ErrorResponse errorResponse) {
    var message = "";
    if (errorResponse.title != null) {}
    showErrorDialog(context,
        title: "Ups somenthing went wrong", message: message);
  }
}
