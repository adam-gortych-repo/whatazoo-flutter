import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';

class CustomAppBar extends StatelessWidget {
  final int zooId;
  final double _barHeight = 124.0;

  const CustomAppBar({Key key, this.zooId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final double statusbarHeight = MediaQuery.of(context).padding.top;

    return new Container(
      padding: new EdgeInsets.only(top: statusbarHeight),
      height: statusbarHeight + _barHeight,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: 16),
                BackButton(
                  color: AppConstants.get().sweetPink,
                ),
                SizedBox(height: 16),
              ],
            ),
            GestureDetector(
              onTap: () => ApiService.get().visitZoo(zooId),
              child: Padding(
                padding: const EdgeInsets.only(right: 32),
                child: Icon(
                  Icons.favorite_border,
                  color: AppConstants.get().sweetPink,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
