import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/page/splash_page.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/routing_util.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: AppConstants.get().appName,
      theme: ThemeData(
        primarySwatch: AppConstants.get().primaryColor,
        fontFamily: 'Popppins',
        accentColor: AppConstants.get().accentColor,
        appBarTheme: AppBarTheme(
          color: AppConstants.appBarColor,
          iconTheme: IconThemeData(
            color: AppConstants.get().accentBlack,
          ),
        ),
      ),
      home: SplashPage(title: 'Splash Page'),
      routes: RoutingUtils.allRoutes,
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
    );
  }
}
