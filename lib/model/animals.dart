class Animals {
  int id;
  String name;
  int zooId;

  Animals({this.id, this.name, this.zooId});

  Animals.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    zooId = json['zoo_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['zoo_id'] = this.zooId;
    return data;
  }
}
