import 'package:whatazoo_flutter/model/animals.dart';

class ZooResponse {
  bool success;
  String message;
  Data data;

  ZooResponse({this.success, this.message, this.data});

  ZooResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Zoos> zoos;

  Data({this.zoos});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['zoos'] != null) {
      zoos = new List<Zoos>();
      json['zoos'].forEach((v) {
        zoos.add(new Zoos.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.zoos != null) {
      data['zoos'] = this.zoos.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Zoos {
  int id;
  String name;
  double latitude;
  double longitude;
  String address;
  String description;
  String wikiLink;
  String webpageLink;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  List<Animals> animals;

  Zoos(
      {this.id,
      this.name,
      this.latitude,
      this.longitude,
      this.address,
      this.description,
      this.wikiLink,
      this.webpageLink,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.animals});

  Zoos.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    address = json['address'];
    description = json['description'];
    wikiLink = json['wiki_link'];
    webpageLink = json['webpage_link'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    if (json['animals'] != null) {
      animals = new List<Animals>();
      json['animals'].forEach((v) {
        animals.add(new Animals.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['address'] = this.address;
    data['description'] = this.description;
    data['wiki_link'] = this.wikiLink;
    data['webpage_link'] = this.webpageLink;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.animals != null) {
      data['animals'] = this.animals.map((v) => v.toJson()).toList();
    }
    return data;
  }
}
