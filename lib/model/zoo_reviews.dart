import 'package:whatazoo_flutter/model/animals.dart';

class ZooReviewsResponse {
  bool success;
  String message;
  Data data;

  ZooReviewsResponse({this.success, this.message, this.data});

  ZooReviewsResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  Zoo zoo;
  List<Reviews> reviews;

  Data({this.zoo, this.reviews});

  Data.fromJson(Map<String, dynamic> json) {
    zoo = json['zoo'] != null ? new Zoo.fromJson(json['zoo']) : null;
    if (json['reviews'] != null) {
      reviews = new List<Reviews>();
      json['reviews'].forEach((v) {
        reviews.add(new Reviews.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.zoo != null) {
      data['zoo'] = this.zoo.toJson();
    }
    if (this.reviews != null) {
      data['reviews'] = this.reviews.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Zoo {
  int id;
  String name;
  double latitude;
  double longitude;
  String address;
  String description;
  String wikiLink;
  String webpageLink;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  String averageRating;
  List<Animals> animals;

  Zoo(
      {this.id,
      this.name,
      this.latitude,
      this.longitude,
      this.address,
      this.description,
      this.wikiLink,
      this.webpageLink,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.averageRating,
      this.animals});

  Zoo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    address = json['address'];
    description = json['description'];
    wikiLink = json['wiki_link'];
    webpageLink = json['webpage_link'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    averageRating = json['averageRating'];
    if (json['animals'] != null) {
      animals = new List<Animals>();
      json['animals'].forEach((v) {
        animals.add(new Animals.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['address'] = this.address;
    data['description'] = this.description;
    data['wiki_link'] = this.wikiLink;
    data['webpage_link'] = this.webpageLink;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    data['averageRating'] = this.averageRating;
    if (this.animals != null) {
      data['animals'] = this.animals.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Reviews {
  int id;
  String review;
  int rating;
  int userId;
  int zooId;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  Author author;

  Reviews(
      {this.id,
      this.review,
      this.rating,
      this.userId,
      this.zooId,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.author});

  Reviews.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    review = json['review'];
    rating = json['rating'];
    userId = json['user_id'];
    zooId = json['zoo_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    author =
        json['author'] != null ? new Author.fromJson(json['author']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['review'] = this.review;
    data['rating'] = this.rating;
    data['user_id'] = this.userId;
    data['zoo_id'] = this.zooId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.author != null) {
      data['author'] = this.author.toJson();
    }
    return data;
  }
}

class Author {
  int id;
  String name;
  String surname;
  String email;
  int isAdmin;
  Null blockedAt;
  String createdAt;
  String updatedAt;
  Null deletedAt;

  Author(
      {this.id,
      this.name,
      this.surname,
      this.email,
      this.isAdmin,
      this.blockedAt,
      this.createdAt,
      this.updatedAt,
      this.deletedAt});

  Author.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    surname = json['surname'];
    email = json['email'];
    isAdmin = json['is_admin'];
    blockedAt = json['blocked_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['surname'] = this.surname;
    data['email'] = this.email;
    data['is_admin'] = this.isAdmin;
    data['blocked_at'] = this.blockedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    return data;
  }
}
