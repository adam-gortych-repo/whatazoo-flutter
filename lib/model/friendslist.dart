class FriendsListResponse {
  bool success;
  String message;
  Data data;

  FriendsListResponse({this.success, this.message, this.data});

  FriendsListResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Friends> friends;

  Data({this.friends});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['friends'] != null) {
      friends = new List<Friends>();
      json['friends'].forEach((v) {
        friends.add(new Friends.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.friends != null) {
      data['friends'] = this.friends.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Friends {
  int id;
  String name;
  String surname;
  String email;
  int isAdmin;
  Null blockedAt;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  Pivot pivot;

  Friends(
      {this.id,
      this.name,
      this.surname,
      this.email,
      this.isAdmin,
      this.blockedAt,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.pivot});

  Friends.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    surname = json['surname'];
    email = json['email'];
    isAdmin = json['is_admin'];
    blockedAt = json['blocked_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['surname'] = this.surname;
    data['email'] = this.email;
    data['is_admin'] = this.isAdmin;
    data['blocked_at'] = this.blockedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.pivot != null) {
      data['pivot'] = this.pivot.toJson();
    }
    return data;
  }
}

class Pivot {
  int userId;
  int friendId;

  Pivot({this.userId, this.friendId});

  Pivot.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    friendId = json['friend_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['friend_id'] = this.friendId;
    return data;
  }
}
