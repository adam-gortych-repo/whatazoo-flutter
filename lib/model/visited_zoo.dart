class VisitedZooResponse {
  bool success;
  String message;
  Data data;

  VisitedZooResponse({this.success, this.message, this.data});

  VisitedZooResponse.fromJson(Map<String, dynamic> json) {
    success = json['success'];
    message = json['message'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['success'] = this.success;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<Visited> visited;

  Data({this.visited});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['visited'] != null) {
      visited = new List<Visited>();
      json['visited'].forEach((v) {
        visited.add(new Visited.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.visited != null) {
      data['visited'] = this.visited.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Visited {
  int id;
  String name;
  double latitude;
  double longitude;
  String address;
  String description;
  String wikiLink;
  String webpageLink;
  String createdAt;
  String updatedAt;
  Null deletedAt;
  Pivot pivot;

  Visited(
      {this.id,
      this.name,
      this.latitude,
      this.longitude,
      this.address,
      this.description,
      this.wikiLink,
      this.webpageLink,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.pivot});

  Visited.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    address = json['address'];
    description = json['description'];
    wikiLink = json['wiki_link'];
    webpageLink = json['webpage_link'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    deletedAt = json['deleted_at'];
    pivot = json['pivot'] != null ? new Pivot.fromJson(json['pivot']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['address'] = this.address;
    data['description'] = this.description;
    data['wiki_link'] = this.wikiLink;
    data['webpage_link'] = this.webpageLink;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['deleted_at'] = this.deletedAt;
    if (this.pivot != null) {
      data['pivot'] = this.pivot.toJson();
    }
    return data;
  }
}

class Pivot {
  int userId;
  int zooId;

  Pivot({this.userId, this.zooId});

  Pivot.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    zooId = json['zoo_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['zoo_id'] = this.zooId;
    return data;
  }
}
