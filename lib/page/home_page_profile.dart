import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/login_helper.dart';
import 'package:whatazoo_flutter/util/routing_util.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

class HomePageProfile extends StatefulWidget {
  @override
  _HomePageProfileState createState() => _HomePageProfileState();
}

class _HomePageProfileState extends State<HomePageProfile> {
  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(32.0),
      shrinkWrap: true,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(height: 32),
            Text("Mój profil",
                style: StyleHelper.get().headerTextStyle(
                    fontWeight: FontWeight.bold, opacityValue: 1.0)),
            SizedBox(height: 64),
            Container(
              decoration: new BoxDecoration(
                borderRadius: new BorderRadius.all(Radius.circular(24)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey[300],
                    blurRadius: 6.0,
                    offset: new Offset(0.0, 1.0),
                  ),
                ],
              ),
              child: ClipRRect(
                borderRadius: new BorderRadius.all(Radius.circular(24)),
                child: Image.network(
                  'https://upload.wikimedia.org/wikipedia/commons/b/b2/Sashasalinas.jpg',
                  fit: BoxFit.cover,
                  height: 160,
                  width: 160,
                ),
              ),
            ),
            SizedBox(height: 16),
            Text(LoginHelper.loggedUser.data.user.name,
                style: StyleHelper.get().headlineTextStyle(
                    fontWeight: FontWeight.w300, opacityValue: 1.0)),
            SizedBox(height: 32),
            Text(LoginHelper.loggedUser.data.user.surname,
                style: StyleHelper.get().regularTextStyle(
                    fontWeight: FontWeight.w300, opacityValue: 1.0)),
            SizedBox(height: 16),
            Text(LoginHelper.loggedUser.data.user.createdAt,
                style: StyleHelper.get().regularTextStyle(
                    fontWeight: FontWeight.w300, opacityValue: 1.0)),
            SizedBox(height: 16),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                _showFriendsButton(),
                SizedBox(
                  width: 15,
                ),
                _showAllUsersButton(),
              ],
            ),
          ],
        ),
      ],
    );
  }

  Widget _showAllUsersButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
      child: ClipRRect(
        borderRadius: new BorderRadius.all(Radius.circular(16)),
        child: MaterialButton(
          minWidth: 100.0,
          height: 45,
          color: AppConstants.get().sweetPink,
          child: new Text('Społeczność',
              style: new TextStyle(fontSize: 16.0, color: Colors.white)),
          onPressed: () {
            RoutingUtils.goToUsersListPage(context);
          },
        ),
      ),
    );
  }

  Widget _showFriendsButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
      child: ClipRRect(
        borderRadius: new BorderRadius.all(Radius.circular(16)),
        child: MaterialButton(
          minWidth: 100.0,
          height: 45,
          color: AppConstants.get().sweetPink,
          child: new Text('Znajomi',
              style: new TextStyle(fontSize: 16.0, color: Colors.white)),
          onPressed: () {
            RoutingUtils.goToFriendsListPage(context);
          },
        ),
      ),
    );
  }
}
