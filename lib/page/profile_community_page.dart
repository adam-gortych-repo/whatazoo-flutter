import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/model/users.dart';
import 'package:whatazoo_flutter/page/profile_page.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/common_widgets.dart';
import 'package:whatazoo_flutter/util/login_helper.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

class ProfileUsersPage extends StatefulWidget {
  @override
  _ProfileUsersPageState createState() => _ProfileUsersPageState();
}

class _ProfileUsersPageState extends State<ProfileUsersPage> {
  StreamController<UsersResponse> usersListController =
      StreamController<UsersResponse>();
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    ApiService.get().getUsersList().then(
      (users) {
        setState(
          () {
            usersListController.add(users);
            LoginHelper.usersListData = users;
            isLoading = false;
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    usersListController?.close();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Społeczność",
              style: StyleHelper.get().headlineTextStyle(
                  fontWeight: FontWeight.bold, opacityValue: 1.0)),
          iconTheme: IconThemeData(color: AppConstants.get().sweetPink),
        ),
        body: isLoading ? CommonWidgets.modalLoading() : _buildCommunityBody(),
      ),
    );
  }

  Widget _buildCommunityBody() {
    return StreamBuilder<UsersResponse>(
      stream: usersListController.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData)
          return ListView.builder(
            shrinkWrap: true,
            itemCount: snapshot.data.data.users.length,
            itemBuilder: (BuildContext ctxt, int index) =>
                _buildFriendsContainer(snapshot.data.data.users[index]),
          );
        return Container();
      },
    );
  }

  Container _buildFriendsContainer(Users users) => Container(
        child: Column(
          children: <Widget>[
            Divider(thickness: 0.5),
            SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  GestureDetector(
                    child: Text(users.name,
                        style: StyleHelper.get().regularTextStyle(
                            fontWeight: FontWeight.w300, opacityValue: 1.0)),
                    onTap: () => Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (context) => ProfilePage(
                          users: users,
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () => ApiService.get().addFriend(users.id),
                    child: Icon(
                      Icons.add,
                      color: AppConstants.get().sweetPink,
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 16),
          ],
        ),
      );
}
