import 'dart:async';

import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/model/visited_zoo.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

class HomePageFavorites extends StatefulWidget {
  @override
  _HomePageFavoritesState createState() => _HomePageFavoritesState();
}

class _HomePageFavoritesState extends State<HomePageFavorites> {
  Stream zoos2;
  StreamController<VisitedZooResponse> controller =
      StreamController<VisitedZooResponse>();

  @override
  void initState() {
    super.initState();
    ApiService.get().getVisitedZooList().then(
      (zoos) {
        setState(
          () {
            controller.add(zoos);
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    controller?.close();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      shrinkWrap: true,
      children: <Widget>[
        SizedBox(height: 32),
        Center(
          child: Text("Moje ulubione zoo",
              style: StyleHelper.get().headlineTextStyle(
                  fontWeight: FontWeight.bold, opacityValue: 1.0)),
        ),
        SizedBox(height: 32),
        _buildFavoritesBody(),
      ],
    );
  }

  Widget _buildFavoritesBody() {
    return StreamBuilder<VisitedZooResponse>(
      stream: controller.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData)
          return ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: snapshot.data.data.visited.length,
            itemBuilder: (BuildContext ctx, int index) =>
                _buildVisitedContainer(snapshot.data.data.visited[index]),
          );
        return Container();
      },
    );
  }

  Container _buildVisitedContainer(Visited visited) => Container(
        child: Column(
          children: <Widget>[
            Divider(thickness: 0.5),
            SizedBox(height: 16),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    visited.name,
                    style: StyleHelper.get().regularTextStyle(
                        fontWeight: FontWeight.w300, opacityValue: 1.0),
                  ),
                  Icon(
                    Icons.delete,
                    color: AppConstants.get().sweetPink,
                  )
                ],
              ),
            ),
            SizedBox(height: 16),
          ],
        ),
      );
}
