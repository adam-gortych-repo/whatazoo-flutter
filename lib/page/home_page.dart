import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/model/zoo.dart';
import 'package:whatazoo_flutter/page/home_page_favorites.dart';
import 'package:whatazoo_flutter/page/home_page_profile.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';

import 'home_page_map.dart';

class HomePage extends StatefulWidget {
  final String title;

  const HomePage({Key key, this.title}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  Zoos currentZoo;

  final List<Widget> _children = [
    HomePageMap(),
    HomePageFavorites(),
    HomePageProfile(),
  ];

  void _onItemTapped(int index) {
    setState(
      () {
        _selectedIndex = index;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          bottomNavigationBar: _buildBottomNavigationBar(),
          body: _children[_selectedIndex]),
    );
  }

  BottomNavigationBar _buildBottomNavigationBar() {
    return BottomNavigationBar(
      backgroundColor: AppConstants.get().white,
      elevation: 0,
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.bookmark_border),
          title: Text('Mapa'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.favorite_border),
          title: Text('Ulubione'),
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.people),
          title: Text('Społeczność'),
        ),
      ],
      currentIndex: _selectedIndex,
      selectedItemColor: AppConstants.get().sweetPink,
      onTap: _onItemTapped,
    );
  }
}
