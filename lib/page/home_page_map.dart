import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/model/zoo.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/routing_util.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

class HomePageMap extends StatefulWidget {
  @override
  _HomePageMapState createState() => _HomePageMapState();
}

class _HomePageMapState extends State<HomePageMap> {
  String _mapStyle;
  ZooResponse _zoos;
  Set<Marker> _markers = {};
  Zoos currentZoo;
  String zooImageUrl;
  bool isLoading = false;

  Future<ZooResponse> zoosFuture;

  static const LatLng _center = const LatLng(51.107883, 17.038538);
  LatLng pinPosition = LatLng(51.107883, 17.038538);

  @override
  void initState() {
    super.initState();
    ApiService.get().getZooList().then(
      (zoos) {
        setState(
          () {
            _zoos = zoos;
            setupAllMarkers();
          },
        );
      },
    );

    rootBundle.loadString('assets/light.txt').then(
      (string) {
        _mapStyle = string;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    GoogleMapController mapController;
    return Stack(
      alignment: Alignment.bottomCenter,
      children: <Widget>[
        _buildGoogleMap(mapController),
        Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            //buildWhiteShadow(),
            _buildZooAsset(currentZoo),
          ],
        ),
        Align(
          alignment: Alignment.topRight,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: _buildIconsColumn(),
          ),
        ),
      ],
    );
  }

  Column _buildIconsColumn() {
    return Column(
      children: <Widget>[
        Container(
          height: 46,
          width: 46,
          decoration: new BoxDecoration(
            color: AppConstants.get().white,
            borderRadius: new BorderRadius.all(Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey[400],
                blurRadius: 2.0,
                offset: new Offset(0.0, 1.0),
              ),
            ],
          ),
          child: Icon(
            Icons.search,
            color: AppConstants.get().sweetPink,
          ),
        ),
        SizedBox(height: 16),
        Container(
          height: 46,
          width: 46,
          decoration: new BoxDecoration(
            color: AppConstants.get().white,
            borderRadius: new BorderRadius.all(Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey[400],
                blurRadius: 2.0,
                offset: new Offset(0.0, 1.0),
              ),
            ],
          ),
          child: Icon(
            Icons.navigation,
            color: AppConstants.get().sweetPink,
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(16),
          child: Container(
            height: 46,
            width: 46,
            decoration: new BoxDecoration(
              color: AppConstants.get().white,
              borderRadius: new BorderRadius.all(Radius.circular(20)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey[400],
                  blurRadius: 2.0,
                  offset: new Offset(0.0, 1.0),
                ),
              ],
            ),
            child: Icon(
              Icons.settings,
              color: AppConstants.get().sweetPink,
            ),
          ),
        ),
      ],
    );
  }

  GoogleMap _buildGoogleMap(GoogleMapController mapController) {
    return GoogleMap(
      initialCameraPosition: CameraPosition(
        target: _center,
        zoom: 11.0,
      ),
      markers: _markers,
      onMapCreated: (GoogleMapController controller) {
        mapController = controller;
        mapController.setMapStyle(_mapStyle);
      },
    );
  }

  Container buildWhiteShadow() {
    return Container(
      height: 200.0,
      decoration: BoxDecoration(
        color: Colors.white,
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomLeft,
          colors: [
            Colors.white.withOpacity(0.0),
            Colors.white.withOpacity(0.1),
            Colors.white.withOpacity(0.4),
            Colors.white.withOpacity(0.5),
            Colors.white.withOpacity(0.7),
            Colors.white.withOpacity(1.0),
            AppConstants.get().white,
          ],
        ),
      ),
    );
  }

  Widget _buildZooAsset(Zoos zoos) {
    if (zoos != null)
      return GestureDetector(
        onTap: () => RoutingUtils.goToZooDetails(context, zoos, zooImageUrl),
        child: Container(
          height: 220,
          child: Stack(
            alignment: Alignment.topLeft,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    bottom: 32, left: 18, right: 18, top: 56),
                child: Container(
                  width: 200,
                  decoration: new BoxDecoration(
                      color: AppConstants.get().white,
                      borderRadius: new BorderRadius.all(Radius.circular(32)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey[200],
                          blurRadius: 2.0,
                          offset: new Offset(0.0, 1.0),
                        ),
                      ]),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(left: 36, top: 32, right: 16),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(height: 32),
                        Text(zoos.name ?? "",
                            maxLines: 1,
                            style: StyleHelper.get().headlineTextStyle(
                                fontWeight: FontWeight.w600)),
                        SizedBox(height: 8),
                        Text(zoos.address ?? "",
                            maxLines: 1,
                            style: StyleHelper.get().regularTextStyle(
                                fontWeight: FontWeight.w300,
                                opacityValue: 0.6)),
                      ],
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 48),
                child: Align(
                  widthFactor: 40,
                  alignment: Alignment.topLeft,
                  child: ClipRRect(
                    borderRadius: new BorderRadius.all(Radius.circular(32)),
                    child: Image.network(
                      zooImageUrl ?? "",
                      fit: BoxFit.cover,
                      height: 112,
                      width: 112,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    return Container();
  }

  void setupAllMarkers() {
    for (Zoos zoo in _zoos?.data?.zoos) {
      _markers.add(
        Marker(
            markerId: MarkerId(zoo.id.toString()),
            position: LatLng(zoo.latitude, zoo.longitude),
            onTap: () {
              setState(() {
                zooImageUrl = (AppConstants.get().randomImage..shuffle()).first;
                currentZoo = zoo;
              });
            }),
      );
    }
  }
}
