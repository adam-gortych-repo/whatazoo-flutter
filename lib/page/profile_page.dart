import 'dart:async';

import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/model/user_feed.dart';
import 'package:whatazoo_flutter/model/users.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

class ProfilePage extends StatefulWidget {
  final Users users;

  const ProfilePage({Key key, this.users}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  StreamController<UserFeed> controller = StreamController<UserFeed>();
  UserFeed userFeed;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(color: AppConstants.get().sweetPink),
          title: Text(widget.users.surname,
              style: StyleHelper.get().headlineTextStyle(
                  fontWeight: FontWeight.bold, opacityValue: 1.0))),
      body: ListView(
        padding: const EdgeInsets.all(32.0),
        shrinkWrap: true,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: 32),
              Container(
                decoration: new BoxDecoration(
                  borderRadius: new BorderRadius.all(Radius.circular(24)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[300],
                      blurRadius: 6.0,
                      offset: new Offset(0.0, 1.0),
                    ),
                  ],
                ),
                child: ClipRRect(
                  borderRadius: new BorderRadius.all(Radius.circular(24)),
                  child: Image.network(
                    'https://d.wpimg.pl/1989216604-1900795938/donald-trump.jpg',
                    fit: BoxFit.cover,
                    height: 160,
                    width: 160,
                  ),
                ),
              ),
              SizedBox(height: 16),
              Text(widget.users.name,
                  style: StyleHelper.get().headlineTextStyle(
                      fontWeight: FontWeight.w300, opacityValue: 1.0)),
              SizedBox(height: 32),
              Text(widget.users.surname,
                  style: StyleHelper.get().regularTextStyle(
                      fontWeight: FontWeight.w300, opacityValue: 1.0)),
              SizedBox(height: 16),
              Text(widget.users.createdAt,
                  style: StyleHelper.get().regularTextStyle(
                      fontWeight: FontWeight.w300, opacityValue: 1.0)),
            ],
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    ApiService.get().getUserFeed(widget.users.id).then(
      (feed) {
        setState(
          () {
            controller.add(feed);
            feed = userFeed;
          },
        );
      },
    );
  }
}
