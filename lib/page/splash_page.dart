import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/routing_util.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

class SplashPage extends StatefulWidget {
  final String title;

  const SplashPage({Key key, this.title}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                AppConstants.get().white,
                AppConstants.get().white,
                AppConstants.get().accentWhite,
              ],
            ),
          ),
          child: _buildSplashBody(),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 3), () => navigateToNextPage());
  }

  Center _buildSplashBody() {
    return Center(
      child: Container(
        height: 400,
        width: 400,
        decoration: BoxDecoration(
            color: AppConstants.get().white,
            borderRadius: BorderRadius.all(Radius.circular(48))),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(AppConstants.get().brandTextLogo,
                style: StyleHelper.get()
                    .headerTextStyle(fontWeight: FontWeight.bold)),
            SizedBox(height: 32),
            Container(
                height: 128,
                width: 128,
                decoration: BoxDecoration(shape: BoxShape.circle, boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 3.0,
                    offset: new Offset(1.0, 1.0),
                  ),
                ]),
                child: ClipOval(
                  child: Image.network(
                    AppConstants.get().placeholder,
                    fit: BoxFit.cover,
                  ),
                )),
            SizedBox(height: 32),
            Text("whatAZoo",
                style: StyleHelper.get()
                    .headlineTextStyle(fontWeight: FontWeight.bold)),
            SizedBox(height: 16),
          ],
        ),
      ),
    );
  }

  void navigateToNextPage() {
    Navigator.of(context).pushReplacementNamed(RoutingUtils.login);
  }
}
