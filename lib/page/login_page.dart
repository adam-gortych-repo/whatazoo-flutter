import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/communication/error_reponse.dart';
import 'package:whatazoo_flutter/model/user.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/common_widgets.dart';
import 'package:whatazoo_flutter/util/login_helper.dart';
import 'package:whatazoo_flutter/util/routing_util.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;
  var emailController = TextEditingController();
  var passwordController = TextEditingController();
  var emailHint = "email";
  var passwordHint = "hasło";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppConstants.get().accentWhite,
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                AppConstants.get().white,
                AppConstants.get().white,
                AppConstants.get().white,
                AppConstants.get().accentWhite,
              ],
            ),
          ),
          child: Stack(
            children: <Widget>[
              ListView(
                padding: const EdgeInsets.all(16.0),
                shrinkWrap: true,
                children: <Widget>[
                  SizedBox(height: 64),
                  _buildHeader(),
                  SizedBox(height: 48),
                  _buildInputFields(),
                  SizedBox(height: 128),
                  Container(
                    height: 50,
                    width: 400,
                    child: MaterialButton(
                      color: AppConstants.get().sweetPink,
                      child: Text(
                        "Zaloguj",
                        style: TextStyle(color: Colors.white, fontSize: 16),
                      ),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                      ),
                      onPressed: () => _login(context),
                    ),
                  ),
                  SizedBox(height: 16),
                ],
              ),
              _isLoading ? CommonWidgets.modalLoading() : new Container()
            ],
          ),
        ),
      ),
    );
  }

  Column _buildHeader() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.pets,
          color: AppConstants.get().sweetPink,
          size: 48,
        ),
        SizedBox(height: 32),
        Text("Witaj!",
            style:
                StyleHelper.get().headerTextStyle(fontWeight: FontWeight.bold)),
        SizedBox(height: 8),
        Text("Zaloguj sie, aby kontynuowac",
            style: StyleHelper.get().headlineGreyTextStyle()),
      ],
    );
  }

  Column _buildInputFields() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 8),
        CommonWidgets.getInputField(
            TextInputType.emailAddress, emailController, emailHint, emailHint),
        SizedBox(height: 32),
        CommonWidgets.getInputField(
            TextInputType.text, passwordController, passwordHint, passwordHint,
            hideText: true),
      ],
    );
  }

  _login(BuildContext context) {
    setState(() {
      _isLoading = true;
    });
    ApiService.get().login(emailController.text, passwordController.text).then(
      (value) {
        setState(() {
          _isLoading = false;
        });
        if (value is UserData) {
          LoginHelper.loggedUser = value;
          ApiService.TOKEN = value.data.token;

          Navigator.of(context).pushReplacementNamed(RoutingUtils.home);
        } else if (value is ErrorResponse) {
          RoutingUtils.showErrorResponseDialog(context, value);
        }
      },
    );
  }

  void navigateToNextPage() {
    Navigator.of(context).pushReplacementNamed(RoutingUtils.home);
  }
}
