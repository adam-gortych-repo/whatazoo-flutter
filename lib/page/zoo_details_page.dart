import 'dart:async';

import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/model/animals.dart';
import 'package:whatazoo_flutter/model/zoo.dart';
import 'package:whatazoo_flutter/model/zoo_reviews.dart' as prefix0;
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';
import 'package:whatazoo_flutter/widgets/custom_appbar.dart';

class ZooDetailsPage extends StatefulWidget {
  final String title;
  final Zoos zoos;
  final String zooImageUrl;

  const ZooDetailsPage({Key key, this.title, this.zoos, this.zooImageUrl})
      : super(key: key);

  @override
  _ZooDetailsPageState createState() => _ZooDetailsPageState();
}

class _ZooDetailsPageState extends State<ZooDetailsPage> {
  StreamController<prefix0.ZooReviewsResponse> zooReviewsController =
      StreamController<prefix0.ZooReviewsResponse>();
  bool isLoading = true;
  final _enTextControler = TextEditingController();
  String _exerciseName;
  bool _validate = false;

  @override
  void initState() {
    super.initState();
    ApiService.get().getZooReviews(widget.zoos.id.toString()).then(
      (zooReviews) {
        setState(
          () {
            zooReviewsController.add(zooReviews);
            isLoading = false;
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    zooReviewsController?.close();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: ListView(
          shrinkWrap: true,
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                    width: MediaQuery.of(context).size.width,
                    foregroundDecoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [
                          AppConstants.get().white.withOpacity(0.2),
                          AppConstants.get().white.withOpacity(0.4),
                          AppConstants.get().white.withOpacity(0.6),
                          AppConstants.get().white.withOpacity(0.8),
                          AppConstants.get().white.withOpacity(0.9),
                          AppConstants.get().white,
                        ],
                      ),
                    ),
                    child: Image.network(widget.zooImageUrl ?? "")),
                CustomAppBar(
                  zooId: widget.zoos.id,
                ),
                _buildZooAsset(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildZooAsset() {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 64, right: 48),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 124),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 32),
                  Text(widget.zoos.name,
                      style: StyleHelper.get()
                          .headlineTextStyle(fontWeight: FontWeight.w600)),
                  SizedBox(height: 8),
                  Text(
                    widget.zoos.description ??
                        "Jeden z najładniejszych ogrodów "
                            "zoologicznych w Polsce. Wart wizyty.",
                    maxLines: 3,
                    style: StyleHelper.get().regularTextStyle(
                        fontWeight: FontWeight.w300, opacityValue: 1.0),
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: 16),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.pin_drop,
                        color: AppConstants.get().sweetPink,
                      ),
                      SizedBox(width: 8),
                      Expanded(
                        child: Text(
                          widget.zoos.address,
                          style: StyleHelper.get()
                              .regularTextStyle(fontWeight: FontWeight.w300),
                          overflow: TextOverflow.fade,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 48),
                  Text("Atrakcje",
                      style: StyleHelper.get()
                          .headlineTextStyle(fontWeight: FontWeight.w600)),
                ],
              ),
            ],
          ),
        ),
        _buildAnimalsList(),
        Text("Komentarze",
            style: StyleHelper.get()
                .headlineTextStyle(fontWeight: FontWeight.w600)),
        _buildReviewsStreamBuilder(),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: _addReviewButton(),
        ),
      ],
    );
  }

  StreamBuilder<prefix0.ZooReviewsResponse> _buildReviewsStreamBuilder() {
    return StreamBuilder<prefix0.ZooReviewsResponse>(
        stream: zooReviewsController.stream,
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null)
            return Container(
              height: 300,
              child: ListView.builder(
                  // shrinkWrap: true,
                  itemCount: snapshot.data.data.reviews.length,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: ListTile(
                        isThreeLine: true,
                        title:
                            Text(snapshot.data.data.reviews[index].author.name),
                        subtitle:
                            Text(snapshot.data.data.reviews[index].review),
                        leading: CircleAvatar(
                          radius: 34,
                        ),
                      ),
                    );
                  }),
            );
          return Container();
        });
  }

  Container _buildAnimals(Animals animals) => Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Text(animals.name,
                style: StyleHelper.get().regularTextStyle(
                    fontWeight: FontWeight.w300, opacityValue: 1.0)),
            ClipRRect(
              borderRadius: new BorderRadius.all(Radius.circular(16)),
              child: Image.network(
                widget.zooImageUrl ?? "",
                fit: BoxFit.cover,
                height: 64,
                width: 144,
              ),
            ),
          ],
        ),
      );

  Widget _buildAnimalsList() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 24.0),
      height: MediaQuery.of(context).size.height * 0.25,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: widget.zoos.animals.length,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: MediaQuery.of(context).size.width * 0.5,
                decoration: BoxDecoration(
                  borderRadius: new BorderRadius.all(Radius.circular(16)),
                  color: AppConstants.get().white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey[200],
                      spreadRadius: 1,
                      blurRadius: 1,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ],
                ),
                child: _buildAnimals(widget.zoos.animals[index]),
              ),
            );
          }),
    );
  }

  Widget _addReviewButton() {
    return Padding(
      padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
      child: ClipRRect(
        borderRadius: new BorderRadius.all(Radius.circular(16)),
        child: MaterialButton(
          minWidth: 100.0,
          height: 45,
          color: AppConstants.get().sweetPink,
          child: new Text('Dodaj komentarz',
              style: new TextStyle(fontSize: 16.0, color: Colors.white)),
          onPressed: () {
            _addDialog(context);
          },
        ),
      ),
    );
  }

  Future<bool> _addDialog(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Dodaj komentarz', style: TextStyle(fontSize: 15.0)),
            content: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _enTextControler,
                    maxLength: 20,
                    decoration: InputDecoration(
                      labelText: 'wpisz komentarz',
                      errorText:
                          _validate ? 'Komentarz nie moze byc pusty' : null,
                    ),
                    onChanged: (value) {
                      this._exerciseName = value;
                    },
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Add'),
                textColor: new Color(0xff7ED321),
                onPressed: () {
                  setState(() {
                    _enTextControler.text.isEmpty
                        ? _validate = true
                        : _validate = false;
                  });
                  if (_validate == false) {
                    ApiService.get()
                        .addReview(_exerciseName, 5, widget.zoos.id);
                  }
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}
