import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:whatazoo_flutter/communication/api_service.dart';
import 'package:whatazoo_flutter/model/friendslist.dart';
import 'package:whatazoo_flutter/util/app_constants.dart';
import 'package:whatazoo_flutter/util/common_widgets.dart';
import 'package:whatazoo_flutter/util/login_helper.dart';
import 'package:whatazoo_flutter/util/style_helper.dart';

class ProfileFriends extends StatefulWidget {
  @override
  _ProfileFriendsState createState() => _ProfileFriendsState();
}

class _ProfileFriendsState extends State<ProfileFriends> {
  StreamController<FriendsListResponse> friendsListController =
      StreamController<FriendsListResponse>();
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    ApiService.get().getFriendsList().then(
      (friends) {
        setState(
          () {
            friendsListController.add(friends);
            LoginHelper.userFriends = friends;
            isLoading = false;
          },
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    friendsListController?.close();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Moi znajomi",
              style: StyleHelper.get().headlineTextStyle(
                  fontWeight: FontWeight.bold, opacityValue: 1.0)),
          iconTheme: IconThemeData(color: AppConstants.get().sweetPink),
        ),
        body: isLoading ? CommonWidgets.modalLoading() : _buildFriendsBody(),
      ),
    );
  }

  Widget _buildFriendsBody() {
    return StreamBuilder<FriendsListResponse>(
      stream: friendsListController.stream,
      builder: (context, snapshot) {
        if (snapshot.hasData)
          return ListView.builder(
            shrinkWrap: true,
            itemCount: snapshot.data.data.friends.length,
            itemBuilder: (BuildContext ctxt, int index) =>
                _buildFriends(snapshot.data.data.friends[index]),
          );
        return Container();
      },
    );
  }

  Container _buildFriends(Friends friends) => Container(
        child: Column(
          children: <Widget>[
            Divider(
              thickness: 0.5,
            ),
            SizedBox(
              height: 16,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(friends.name,
                      style: StyleHelper.get().regularTextStyle(
                          fontWeight: FontWeight.w300, opacityValue: 1.0)),
                  Icon(
                    Icons.delete_forever,
                    color: AppConstants.get().sweetPink,
                  )
                ],
              ),
            ),
            SizedBox(height: 16),
          ],
        ),
      );
}
