class ErrorResponse {
  final bool success;
  final List<ErrorFields> errors;
  final String title;

  ErrorResponse({this.success, this.errors, this.title});

  factory ErrorResponse.fromJson(Map<String, dynamic> json) {
    return ErrorResponse(
      success: json['success'] ?? -1,
      title: json['messsage'] ?? "",
      errors: json['errors']
          ?.map<ErrorFields>((dynamic model) => ErrorFields.fromJson(model))
          ?.toList(),
    );
  }
}

class ErrorFields {
  final String field;
  final String description;

  ErrorFields({this.field, this.description});

  factory ErrorFields.fromJson(Map<String, dynamic> json) {
    return ErrorFields(
      field: json['field'] ?? "",
      description: json['description'] ?? "",
    );
  }
}
