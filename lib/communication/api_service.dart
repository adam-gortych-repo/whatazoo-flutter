import 'dart:async';
import 'dart:convert';

import 'package:whatazoo_flutter/communication/error_reponse.dart';
import 'package:whatazoo_flutter/communication/sucess_response.dart';
import 'package:whatazoo_flutter/model/friendslist.dart';
import 'package:whatazoo_flutter/model/user.dart';
import 'package:whatazoo_flutter/model/user_feed.dart';
import 'package:whatazoo_flutter/model/users.dart';
import 'package:whatazoo_flutter/model/visited_zoo.dart';
import 'package:whatazoo_flutter/model/zoo.dart';
import 'package:whatazoo_flutter/model/zoo_reviews.dart';
import 'package:whatazoo_flutter/util/network_util.dart';

abstract class ApiDefinition {
  Future<dynamic> login(String email, String password);
}

class ApiService implements ApiDefinition {
  NetworkUtil _netUtil = new NetworkUtil();

  static const SCHEMA = "https://";

  static const HOST = "whatazoo.sikorski.dev";
  static const BASE_URL = SCHEMA + HOST + "/api/";

  static const ACCOUNT_LOGIN = BASE_URL + "auth/login";
  static const ACCOUNT_REGISTER = BASE_URL + "auth/register";
  static const ZOO_LIST = BASE_URL + "zoos";
  static const VISITED_ZOO_LIST = BASE_URL + "zoos/visited";
  static const FRIENDSLIST = BASE_URL + "friends";
  static const USERSLIST = BASE_URL + "users";
  static const USER_FEED = BASE_URL + "user/";
  static const ZOO_REVIEWS = BASE_URL + "zoo/";
  static const ZOO_ADD_REVIEW = BASE_URL + "zoo/addReview";
  static const VISIT_ZOO = BASE_URL + "visit/zoo";

  static String TOKEN = "";

  static final ApiService _instance = new ApiService._internal();

  factory ApiService() {
    return _instance;
  }

  static ApiService get() {
    return _instance;
  }

  ApiService._internal() {
    // initialization logic here
  }

  static Map<String, String> getHeaders() {
    return {
      'content-type': 'application/json',
      'Authorization': 'Bearer ' + TOKEN,
      "accept": "application/json",
    };
  }


  Future<dynamic> login(String email, String password) {
    Map<String, String> body = {
      'email': '$email',
      'password': '$password',
    };
    return _netUtil
        .post(ACCOUNT_LOGIN, body: json.encode(body))
        .then((dynamic res) {
      print(res.toString());
      if (res is ErrorResponse) return res;
      return UserData.fromJson(res);
    });
  }

  Future<dynamic> register(String name, String surname, String email,
      String password, String confirmPassword) {
    Map<String, dynamic> body = {
      'name': name,
      'surname': surname,
      'email': email,
      'password': password,
      'confirmPassword': confirmPassword,
    };
    return _netUtil
        .post(ACCOUNT_REGISTER, body: json.encode(body))
        .then((dynamic res) {
      print(res.toString());
      if (res is ErrorResponse) return res;
      return UserData.fromJson(res);
    });
  }

  Future<dynamic> getZooList() async {
    return _netUtil.get(ZOO_LIST).then((dynamic res) {
      if (res is ErrorResponse) return res;
      print(res.toString());
      return ZooResponse.fromJson(res);
    });
  }

  Future<dynamic> getVisitedZooList() async {
    return _netUtil.get(VISITED_ZOO_LIST).then((dynamic res) {
      if (res is ErrorResponse) return res;
      print(res.toString());
      return VisitedZooResponse.fromJson(res);
    });
  }

  Future<dynamic> getFriendsList() async {
    return _netUtil.get(FRIENDSLIST).then((dynamic res) {
      if (res is ErrorResponse) return res;
      print(res.toString());
      return FriendsListResponse.fromJson(res);
    });
  }

  Future<dynamic> getUsersList() async {
    return _netUtil.get(USERSLIST).then((dynamic res) {
      if (res is ErrorResponse) return res;
      print(res.toString());
      return UsersResponse.fromJson(res);
    });
  }

  Future<dynamic> getZooReviews(String zooId) {
    return _netUtil.get(ZOO_REVIEWS + zooId).then((dynamic res) {
      print(res.toString());
      if (res is ErrorResponse) return res;
      return ZooReviewsResponse.fromJson(res);
    });
  }

  Future<dynamic> addReview(String review, int rating, int zooId) {
    Map<String, dynamic> body = {
      'review': review,
      'rating': rating,
      'zoo_id': zooId,
    };

    return _netUtil
        .post(ZOO_ADD_REVIEW, body: json.encode(body))
        .then((dynamic res) {
      print(res.toString());
      if (res is ErrorResponse) return res;
      return SuccessResponse.fromJson(res);
    });
  }

  Future<dynamic> addFriend(int friendId) {
    Map<String, dynamic> body = {
      'friendId': friendId,
    };

    return _netUtil
        .post(ZOO_ADD_REVIEW, body: json.encode(body))
        .then((dynamic res) {
      print(res.toString());
      if (res is ErrorResponse) return res;
      return SuccessResponse.fromJson(res);
    });
  }

  Future<dynamic> visitZoo(int zooId) {
    Map body = {
      'zoo_id': zooId,
    };
    return _netUtil
        .post(VISIT_ZOO, body: json.encode(body))
        .then((dynamic res) {
      print(res.toString());
      if (res is ErrorResponse) return res;
      return SuccessResponse.fromJson(res);
    });
  }

  Future<dynamic> getUserFeed(int userId) {
    String string = (USER_FEED + '${userId.toString()}/news');

    return _netUtil
        .get(string)
        .then((dynamic res) {
      print(res.toString());
      if (res is ErrorResponse) return res;
      return UserFeed.fromJson(res);
    });
  }
}
